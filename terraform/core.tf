terraform {
  required_version = ">= 0.15"
  backend "s3" {
    bucket  = "terraform.archie-tech.com"
    key     = "devops/dev/terraform.tfstate"
    region  = "eu-central-1"
    profile = "archie-tech"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.50"
    }
  }
}

provider "aws" {
  region                  = "eu-central-1"
  shared_credentials_file = "%USERPROFILE%/.aws/credentials"
  profile                 = "archie-tech"
  default_tags {
    tags = {
      Environment = "Development"
      Application = "DevOps"
      Customer    = "Archie Tech"
      Creator     = "Terraform"
    }
  }
}
